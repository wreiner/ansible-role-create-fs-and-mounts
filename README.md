# Ansible Role: create-fs-and-mounts

Creates filesystems and mounts.

## Usage

To use this module the following config block, typically in the respective host_vars file, is required:

```
filesystem_mappings:
  - device: "/dev/rootvg/prometheus_lv"
    filesystem: "xfs"
    mount_point: "/var/lib/docker/prometheus_data"
    mount_point_owner: "root"
    mount_point_group: "root"
    mount_point_mode: "0700"
```

After adding the config block, add the module to the playbook:

```
  roles:
    [..]
    - create-fs-and-mounts
    [..]
```
